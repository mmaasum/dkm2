module.exports = {
  name: 'geo-library',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/geo-library',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
