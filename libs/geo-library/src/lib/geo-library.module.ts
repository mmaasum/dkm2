import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Route } from '@angular/router';
import { MapViewComponent } from './map-view/map-view.component';
import { PickLocationComponent } from './pick-location/pick-location.component';


import { BrowserModule } from '@angular/platform-browser';
// import { NgModule } from '@angular/core';

// import { AppComponent } from './app.component';
// import { GeoLibraryModule, geoLibraryRoutes } from '@dkm/geo-library';
// import { Routes, RouterModule } from '@angular/router';
import { AdminModule, adminRoutes } from '@dkm/admin';
// import { MapModule } from './map/map.module';
// import { MaterialModule } from './material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
// import { DisplayLocationComponent } from './map/display-location/display-location.component'
import '@angular/compiler';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button'

export const geoLibraryRoutes: Route[] = [
  // {path:'', component: PickLocationComponent},
  {path:'map-view', component: MapViewComponent},
  {path:'pick-location', component: PickLocationComponent} 
];

@NgModule({
  imports: [CommonModule, RouterModule,
    
  ],
  declarations: [MapViewComponent, PickLocationComponent]
})
export class GeoLibraryModule {}
