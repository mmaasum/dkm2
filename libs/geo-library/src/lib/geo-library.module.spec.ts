import { async, TestBed } from '@angular/core/testing';
import { GeoLibraryModule } from './geo-library.module';

describe('GeoLibraryModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [GeoLibraryModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(GeoLibraryModule).toBeDefined();
  });
});
