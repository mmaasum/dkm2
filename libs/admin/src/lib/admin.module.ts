import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Route } from '@angular/router';
import { UserCreateComponent } from './user-create/user-create.component';
import {MatCardModule} from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule} from '@angular/material/radio';
import {MatButtonModule} from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressBarModule} from '@angular/material/progress-bar';
// import {MatSelectModule} from '@angular/material/select';


export const adminRoutes: Route[] = [
  {path:'fake-input', component: UserCreateComponent}
];

@NgModule({
  imports: [CommonModule, RouterModule,MatSelectModule,MatRadioModule,MatButtonModule,
    MatDatepickerModule,MatIconModule,MatProgressBarModule,
    MatCardModule, MatInputModule],
  declarations: [UserCreateComponent]
})
export class AdminModule {}
