import { Component, OnInit } from '@angular/core';
import * as md from 'md.icons'
@Component({
  selector: 'dkm-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {

  constructor() { }
  files:[];
  fileName:'Upload File';
  ngOnInit(): void {
  }
  previewImage(event){
    console.log(event.target.files[0].name);
    this.fileName=event.target.files[0].name;
    console.log(this.fileName);
  }
  onClick(){}
}
