import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Subject } from 'rxjs';
import * as L from 'leaflet';

export class Location {
  name: string;
  details: string;
  location: string;
  file:any;
}

@Component({
  selector: 'dkm-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {

  constructor( private _formBuilder: FormBuilder) { 
    this._unsubscribeAll = new Subject();
  }
  form: FormGroup;
  latitude
  longitude 
  mLat:string;
  mLng:string;
  coordinate:string;
  tobeDelete:string;
  tasks: Location[] = [];
  private map;
  private _unsubscribeAll: Subject<any>;
  files:[];
  fileName:'Upload File';

  tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 13, attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      });
  
    icon = {
      icon: L.icon({
        iconSize: [ 25, 41 ],
        iconAnchor: [ 13, 0 ],
        iconUrl: '../../../../assets/marker-icon.png',
        shadowUrl: '../../../../assets/marker-shadow.png'
      })
    };
  // object: Object = {foo: 'bar', baz: 'qux', nested: {xyz: 3, numbers: [1, 2, 3, 4, 5]}};

  geoData: any = {type: 'FeatureCollection', features: []};
  feature: any = {type:'Feature', properties:{}, geometry:{type:'Point', coordinates:[]}};
  // geometry: any = {type:'Point', coordinates:[]};
  
  
  cord: any = {coordinates:[]};
  getLatLng(){
          this.map.on('click', (e) => {
        this.mLat = e.latlng.lat;
        this.mLng = e.latlng.lng;
        this.coordinate=this.mLat +','+this.mLng;
        console.log(this.coordinate);
      });
    
  }
  previewImage(event){
    console.log(event.target.files[0].name);
    this.fileName=event.target.files[0].name;
    console.log(this.fileName);
  }

  ngOnInit(): void {

    
    // this.tasks.push({name: 'a', details: 'b', location: 'c', file: 'd'});
    // Reactive Form
    console.log(this.tasks);
    this.form = this._formBuilder.group({
      taskName : ['', Validators.required],
      locationPoint  : ['', Validators.required]
    });
  }

  goLocation(coordinate, task){
    this.latitude=coordinate.split(',')[0];
    this.longitude=coordinate.split(',')[1];

    this.tobeDelete=task;
    console.log(this.tobeDelete);
    const marker = L.marker([this.latitude, this.longitude], this.icon).addTo(this.map);
    // marker.bindPopup('<b>Welcome to!</b><br>'+task).openPopup();
    marker.bindPopup('<b><br>'+task + '<div fxLayout="row" style="height: 250px; width: 250px; border:1px solid black;" ></div>').openPopup();
  }
  
  

  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit(): void {
    this.initMap();
    this.tiles.addTo(this.map);
    const marker = L.marker([32.238359405722655, 35.17822265625], this.icon).addTo(this.map);
    marker.bindPopup('<b>Hello world!</b><br>I am a popup.').openPopup();

    this.getLatLng();
  }
  
  private initMap(): void {
    this.map = L.map('map', {
      center: [32.238359405722655, 35.17822265625],
      zoom: 7
    });
  }

  addTask(name, details, location, file){
    console.log(file);
    this.tasks.push({name: name.value, details: details.value, location: location.value, file: file.value});
      name.value = '';
      details.value = '';
      location.value = '';
      console.log(this.tasks);
  }

}
