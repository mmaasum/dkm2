import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JsonMarkerComponent } from './json-marker.component';

describe('JsonMarkerComponent', () => {
  let component: JsonMarkerComponent;
  let fixture: ComponentFixture<JsonMarkerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JsonMarkerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JsonMarkerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
