import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Subject } from 'rxjs';
import * as L from 'leaflet';

export class Location {
  name: string;
  details: string;
  location: string;
  file:any;
}

@Component({
  selector: 'dkm-json-marker',
  templateUrl: './json-marker.component.html',
  styleUrls: ['./json-marker.component.scss']
})
export class JsonMarkerComponent implements OnInit {

  constructor( private _formBuilder: FormBuilder) { 
    this._unsubscribeAll = new Subject();
  }
  form: FormGroup;
  latitude
  longitude 
  mLat:string;
  mLng:string;
  coordinate:string;
  tobeDelete:string;
  tasks: Location[] = [];
  private map;
  private _unsubscribeAll: Subject<any>;
  files:[];
  fileName:'Upload File';

  tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 13, attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      });
  
    icon = {
      icon: L.icon({
        iconSize: [ 25, 41 ],
        iconAnchor: [ 13, 0 ],
        iconUrl: '../../../../assets/marker-icon.png',
        shadowUrl: '../../../../assets/marker-shadow.png'
      })
    };
  
  geoData: any = {type: 'FeatureCollection', features: []};
  feature: any = {type:'Feature', properties:{}, geometry:{type:'Point', coordinates:[]}};
  // geometry: any = {type:'Point', coordinates:[]};
  
  
  cord: any = {coordinates:[]};
  getLatLng(){
    
    this.map.on('click', (e) => {
      
      const insert = (geoData, index, newItem) => [
        // part of the array before the specified index
        geoData.slice(0, index),
        // inserted item
        newItem,
        // part of the array after the specified index
        geoData.slice(index)
      ]
      this.cord.coordinates.push(e.latlng.lat);
      this.cord.coordinates.push(e.latlng.lng);

      this.feature.geometry.coordinates.push(e.latlng.lng);
      this.feature.geometry.coordinates.push(e.latlng.lat);

      this.geoData.features.push(this.feature);
      // this.feature=null;
      console.log(this.geoData.length);
      
    });
  }
  previewImage(event){
    console.log(event.target.files[0].name);
    this.fileName=event.target.files[0].name;
    console.log(this.fileName);
  }

  ngOnInit(): void {

    this.form = this._formBuilder.group({
      taskName : ['', Validators.required],
      locationPoint  : ['', Validators.required]
    });
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit(): void {
    this.initMap();
    this.tiles.addTo(this.map);
    const marker = L.marker([32.238359405722655, 35.17822265625], this.icon).addTo(this.map);
    this.getLatLng();
  }
  
  private initMap(): void {
    this.map = L.map('map', {
      center: [32.238359405722655, 35.17822265625],
      zoom: 7
    });
  }
}

