import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { BrowserModule } from '@angular/platform-browser';
// import { DisplayLocationComponent } from './display-location/display-location.component';
import { Routes, RouterModule } from '@angular/router';

import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatStepperModule } from '@angular/material/stepper';
import {MatListModule} from '@angular/material/list';
import {MatSidenavModule} from '@angular/material/sidenav';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DisplayLocationComponent } from './display-location/display-location.component';
import { MatCardModule } from '@angular/material/card';
import { JsonMarkerComponent } from './json-marker/json-marker.component';
import { FileUploadComponent } from './file-upload/file-upload.component';

// import { NgxMatFileInputModule } from '@angular-material-components/file-input';
// import { MaterialFileInputModule } from 'ngx-material-file-input';
const routes: Routes = [
  {  path     : 'pick-location', component: DisplayLocationComponent }
];
@NgModule({
  declarations: [DisplayLocationComponent, JsonMarkerComponent, FileUploadComponent],
  imports: [
    MatCardModule,
    RouterModule.forChild(routes),
    CommonModule,
    MatSidenavModule,
    MatListModule,
    CommonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatStepperModule,
    MatButtonModule,
    ReactiveFormsModule,
    FormsModule,
  //   NgxMatFileInputModule,
  //   MaterialFileInputModule
  ],
  exports: [
    CommonModule,
    MatSidenavModule,
    MatListModule,
    CommonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatStepperModule,
    MatButtonModule,
    // NgxMatFileInputModule,
    // MaterialFileInputModule
  ]
})
export class MapModule { }
