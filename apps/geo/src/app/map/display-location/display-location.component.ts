import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Subject } from 'rxjs';
import * as L from 'leaflet';

export class Location {
  name: string;
  details: string;
  location: string;
}

@Component({
  selector: 'dkm-display-location',
  templateUrl: './display-location.component.html',
  styleUrls: ['./display-location.component.scss']
})
export class DisplayLocationComponent implements OnInit {

  constructor( private _formBuilder: FormBuilder) { 
    this._unsubscribeAll = new Subject();
  }
  form: FormGroup;
  latitude
  longitude 
  mLat:string;
  mLng:string;
  coordinate:string;
  tobeDelete:string;
  tasks: Location[] = [];
  private map;
  private _unsubscribeAll: Subject<any>;
 

  tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 13, attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      });
  
    icon = {
      icon: L.icon({
        iconSize: [ 25, 41 ],
        iconAnchor: [ 13, 0 ],
        iconUrl: '../../../../assets/marker-icon.png',
        shadowUrl: '../../../../assets/marker-shadow.png'
      })
    };
  
  
  
  getLatLng(){
          this.map.on('click', (e) => {
        this.mLat = e.latlng.lat;
        this.mLng = e.latlng.lng;
        this.coordinate=this.mLat +','+this.mLng;
        console.log(this.coordinate);
      });
    
  }
  

  ngOnInit(): void {

    console.log(this.tasks);
    this.form = this._formBuilder.group({
      taskName : ['', Validators.required],
      locationPoint  : ['', Validators.required]
    });
  }

  goLocation(coordinate, task){
    this.latitude=coordinate.split(',')[0];
    this.longitude=coordinate.split(',')[1];

    this.tobeDelete=task;
    console.log(this.tobeDelete);
    const marker = L.marker([this.latitude, this.longitude], this.icon).addTo(this.map);
     marker.bindPopup('<b>Welcome to!</b><br>'+task).openPopup();
   // marker.bindPopup('<b><br>'+task + '<div fxLayout="row" style="height: 250px; width: 250px; border:1px solid black;" ></div>').openPopup();
  }
  
  

  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit(): void {
    this.initMap();
    this.tiles.addTo(this.map);
    const marker = L.marker([32.238359405722655, 35.17822265625], this.icon).addTo(this.map);
    marker.bindPopup('<b>Hello world!</b><br>I am a popup.').openPopup();

    this.getLatLng();
  }
  
  private initMap(): void {
    this.map = L.map('map', {
      center: [32.238359405722655, 35.17822265625],
      zoom: 7
    });
  }

  addTask(name, details, location){
    
    this.tasks.push({name: name.value, details: details.value, location: location.value});
      name.value = '';
      details.value = '';
      location.value = '';
      console.log(this.tasks);
  }

}
