// import { Component } from '@angular/core';

import { Component, OnInit } from '@angular/core';


// import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import * as L from 'leaflet';
// import {} from '../assets/'

export class Location {
  name: string;
  details: string;
  location: string;
}

@Component({
  selector: 'dkm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  // form: FormGroup;
  // latitude
  // longitude 
  // mLat:string;
  // mLng:string;
  // coordinate:string;
  // tobeDelete:string;
  // tasks: Location[] = [];
  // private map;
  // private _unsubscribeAll: Subject<any>;

  // tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  //     maxZoom: 13, attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  //     });
  
  //   icon = {
  //     icon: L.icon({
  //       iconSize: [ 25, 41 ],
  //       iconAnchor: [ 13, 0 ],
  //       iconUrl: '../assets/marker-icon.png',
  //       shadowUrl: '../assets/marker-shadow.png'
  //     })
  //   };

  constructor( private _formBuilder: FormBuilder) { 
    // this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    // // Reactive Form
    // this.form = this._formBuilder.group({
    //   taskName : ['', Validators.required],
    //   locationPoint  : ['', Validators.required]
    // });
  }

  // goLocation(coordinate, task){
  //   this.latitude=coordinate.split(',')[0];
  //   this.longitude=coordinate.split(',')[1];

  //   this.tobeDelete=task;
  //   console.log(this.tobeDelete);
  //   const marker = L.marker([this.latitude, this.longitude], this.icon).addTo(this.map);
  //   marker.bindPopup('<b>Welcome to!</b><br>'+task).openPopup();
  // }
  
  // getLatLng(){
    
  //   this.map.on('click', (e) => {
  //     this.mLat = e.latlng.lat;
  //     this.mLng = e.latlng.lng;
  //     this.coordinate=this.mLat +','+this.mLng;
  //     console.log(this.coordinate);
  //   });
  // }

  // // tslint:disable-next-line: use-lifecycle-interface
  // ngAfterViewInit(): void {
  //   this.initMap();
  //   this.tiles.addTo(this.map);
  //   const marker = L.marker([23.777176, 90.3], this.icon).addTo(this.map);
  //   marker.bindPopup('<b>Hello world!</b><br>I am a popup.').openPopup();

  //   this.getLatLng();
  // }
  
  // private initMap(): void {
  //   this.map = L.map('map', {
  //     center: [ 23.777176, 90.399452 ],
  //     zoom: 7
  //   });
  // }

  // addTask(name, details, location){
  //   console.log(name.value);
  //   this.tasks.push({name: name.value, details: details.value, location: location.value});
  //     name.value = '';
  //     details.value = '';
  //     location.value = '';
  //     console.log(this.tasks);
  // }

}

