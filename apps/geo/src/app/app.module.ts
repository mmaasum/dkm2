import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GeoLibraryModule, geoLibraryRoutes } from '@dkm/geo-library';
import { Routes, RouterModule } from '@angular/router';
import { AdminModule, adminRoutes } from '@dkm/admin';
import { MapModule } from './map/map.module';
import { MaterialModule } from './material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
// import { DisplayLocationComponent } from './map/display-location/display-location.component'
import '@angular/compiler';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button'
import {LayoutModule} from '@angular/cdk/layout';
import { LayoutComponent } from './layout/layout.component';
import { HomeComponent } from './home/home.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { HeaderComponent } from './header/header.component';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { SidenavListComponent } from './navigation/sidenav-list/sidenav-list.component';
import { MatMenuModule } from '@angular/material/menu';
import { DisplayLocationComponent } from './map/display-location/display-location.component';
import { AngularSplitModule } from 'angular-split';
import { JsonMarkerComponent } from './map/json-marker/json-marker.component';
import { FileUploadComponent } from './map/file-upload/file-upload.component';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'pick-location', component: DisplayLocationComponent},
  { path: 'json-marker', component: JsonMarkerComponent},
  { path: 'file-upload', component: FileUploadComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'map2', loadChildren: () => import('./map/map.module').then(m => m.MapModule) },
  { path: 'map', children: geoLibraryRoutes },
  { path: 'admin', children: adminRoutes }
];
 
@NgModule({
  declarations: [AppComponent, LayoutComponent, HomeComponent,HeaderComponent, SidenavListComponent],
  imports: [
    AngularSplitModule,
    GeoLibraryModule,
    RouterModule.forRoot(appRoutes),
    AdminModule,
    ReactiveFormsModule,
    FormsModule,
    // MaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
    GeoLibraryModule,
    // RouterModule.forRoot(appRoutes),
    AdminModule,
    // MapModule,
    MatInputModule,
    FlexLayoutModule,
    MatButtonModule,
    

    
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MatTabsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatMenuModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
