module.exports = {
  name: 'geo',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/geo',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
